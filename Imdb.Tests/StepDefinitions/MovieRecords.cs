﻿namespace Imdb.Tests.StepDefinitions
{
    public class MovieRecords
    {
        public string MovieName { get; set; }
        public string Yearofrelease { get; set; }
        public string Plot { get; set; }
        public string Actors { get; set; }
        public string Producer { get; set; }
    }
}