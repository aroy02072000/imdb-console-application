using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Imdb.Services;
using Imdb.Repository;

namespace Imdb.Tests.StepDefinitions
{

    [Binding]
    public class MovieOperationsStepDefinitions
    {
        private readonly ActorService _actorService;
        private readonly ProducerService _producerService;
        private readonly MovieService _movieService;

        private string movieName;
        private string yearOfRelease;
        private string plot;
        private List<string> actorNames;
        private string producerName;
        private string Status;
        private List<List<string>> resultSet;
        private List<MovieRecords> movieRecords;
        private ScenarioContext _scenarioContext;

        public MovieOperationsStepDefinitions(ScenarioContext scenarioContext)
        {
            actorNames = new List<string>();
            _scenarioContext = scenarioContext;
            ActorRepository actorRepository = new ActorRepository();
            ProducerRepository producerRepository = new ProducerRepository();
            _actorService = new ActorService(actorRepository);
            _producerService = new ProducerService(producerRepository);
            _movieService = new MovieService(new MovieRepository(), actorRepository, producerRepository);
        }

        [BeforeScenario(Order = 0)]
        public void AddMockData()
        {
            _actorService.AddMockDataForTesting();
            _producerService.AddMockDataForTesting();
            _movieService.AddMockDataForTesting();
        }

        [BeforeScenario(Order = 1), Scope(Tag = "listMovie")]
        public void ListMovie1()
        {
            _movieService.DeleteMovie("ABC");
            _movieService.DeleteMovie("PQR");
        }

        [BeforeScenario(Order = 2), Scope(Tag = "listMovie", Scenario = "List Movie testing case 2 (when 2 movie record exists)")]
        public void AddMovies()
        {
            movieRecords = new List<MovieRecords>();
            List<string> tempList = new List<string>();
            tempList.Add("Arijit Roy");
            tempList.Add("Arkadeep Roy");
            _movieService.AddMovie("ABC", "2020", "XYZ", tempList, "Bikram Sarkar");
            _movieService.AddMovie("DEF", "2021", "GHI", tempList, "Srijit Basu");
        }

        [Given(@"the choice is (.*)")]
        public void GivenTheChoiceIs(int p0)
        {
            
        }

        [Given(@"the movie name ""([^""]*)"" is taken")]
        public void GivenTheMovieNameIsTaken(string p0)
        {
            movieName = p0;
        }

        [Given(@"the year of release ""([^""]*)"" is taken")]
        public void GivenTheYearOfReleaseIsTaken(string p0)
        {
            yearOfRelease = p0;
        }

        [Given(@"the plot ""([^""]*)"" is taken")]
        public void GivenThePlotIsTaken(string p0)
        {
            plot = p0;
        }

        [Given(@"the actor names ""([^""]*)"" are taken")]
        public void GivenTheActorNamesAreTaken(string p0)
        {
            if (!String.IsNullOrEmpty(p0))
            {
                actorNames.AddRange(p0.Split(','));
            }
        }

        [Given(@"the producer name ""([^""]*)"" is taken")]
        public void GivenTheProducerNameIsTaken(string p0)
        {
            producerName = p0;
        }

        [When(@"I press add movie button")]
        public void WhenIPressAddMovieButton()
        {
            Status = _movieService.AddMovie(movieName, yearOfRelease, plot, actorNames, producerName);
        }

        [Then(@"output should be ""([^""]*)""")]
        public void ThenOutputShouldBe(string status)
        {
            Assert.Equal(status, Status);
        }

        [When(@"I press list movie button")]
        public void WhenIPressListMovieButton()
        {
            resultSet = _movieService.ShowMovies();
        }

        [Then(@"I get ""([^""]*)""")]
        public void ThenIGet(string p0)
        {
            Assert.Equal(p0, resultSet.ElementAtOrDefault(0).ElementAtOrDefault(0));
        }

        [Then(@"the movies in the list should be")]
        public void ThenTheMoviesInTheListShouldBe(Table table)
        {
            foreach (var item in resultSet)
            {
                movieRecords.Add
                (
                    new MovieRecords()
                    {
                        MovieName = item[0],
                        Yearofrelease = item[1],
                        Plot = item[2],
                        Actors = item[3],
                        Producer = item[4]
                    }
                );
            }
            table.CompareToSet<MovieRecords>(movieRecords);
        }

    }
}