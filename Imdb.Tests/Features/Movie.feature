﻿Feature: MovieOperations
	As an admin of IMDb console application
	I want to add a movie to the existing database

# Let us assume DB contains some Actor and Producer records

# Actors:
# Arijit Roy 02/07/2000
# Arkadeep Roy 03/05/1998
# Ankita Datta 17/10/1966
# Soumodip Ghosh 15/08/1982
# Shayani Das 17/03/2010

# Producers:
# Bikram Sarkar 23/01/1956
# Srijit Basu 17/02/1967

# Suppose there is a movie
# Movie Name - ABC
# Year of Release - 2016
# Plot - XYZ
# Actor Names - Arijit Roy, Arkadeep Roy
# Producer Name - Bikram Sarkar 

# We assume all the above comments for the scenarios with the tag @addMovie

#@addMovie
#Scenario: Add movie scenario 1
#	Given the movie name "<moviename>" is taken
#	And the year of release "<year>" is taken
#	And the plot "<plot>" is taken
#	And the actor names "<actornames>" are taken
#	And the producer name "<producername>" is taken
#	When I press add movie button
#	Then output should be "<status>"

@addMovie
Scenario Outline: Add Movie testing assuming the above comments
	Given the choice is 2
	And the movie name "<moviename>" is taken
	And the year of release "<year>" is taken
	And the plot "<plot>" is taken
	And the actor names "<actornames>" are taken
	And the producer name "<producername>" is taken
	When I press add movie button
	Then output should be "<status>"

	Examples: 
	| moviename | year | plot | actornames                  | producername  | status                                                               |
	| PQR       | 2016 | TUV  | Arijit Roy,Arkadeep Roy     | Bikram Sarkar | Database updated successfully...                                     |
	| ABC       | 2017 | XYZ  | Arijit Roy,Ankita Datta     | Srijit Basu   | Movie details already exists...                                      |
	|           | 2010 | GHI  | Soumodip Ghosh,Ankita Datta | Srijit Basu   | Movie name cannot be empty!                                          |
	| DEF       |      | GHI  | Soumodip Ghosh,Ankita Datta | Bikram Sarkar | Year of Release cannot be empty!                                     |
	| DEF       | 2020 |      | Arijit Roy,Soumodip Ghosh   | Bikram Sarkar | Plot cannot be empty!                                                |
	| DEF       | 2018 | GHI  |                             | Srijit Basu   | Must contain atleast one actor details!                              |
	| DEF       | 2017 | GHI  | Arkadeep Roy,Ankita Datta   |               | Producer name cannot be empty!                                       |
	| DEF       | 2011 | GHI  | Shayani Das,Arijit Roy      | Bikram Sarkar | Actor age must be atleast 10 with respect to the Year of Release!    |
	| DEF       | 1980 | GHI  | Ankita Datta                | Srijit Basu   | Producer age must be atleast 35 with respect to the Year of Release! |
	| DEF       | 2020 | GHI  | Arijit Roy,Tanmoy Sarkar    | Bikram Sarkar | Actor record does not exist in the database!                         |
	| DEF       | 2021 | GHI  | Arijit Roy                  | Tanmoy Sarkar | Producer record does not exist in the database!                      |
	| DEF       | 202e | GHI  | Arkadeep Roy,Shayani Das    | Bikram Sarkar | Invalid Year of Release!                                             |


@listMovie
Scenario: List Movie testing case 1 (when no movie record is there)
	When I press list movie button
	Then I get "No Movie record found..."

@listMovie
Scenario: List Movie testing case 2 (when 2 movie record exists)
	When I press list movie button
	Then the movies in the list should be
	| moviename | yearofrelease | plot | actors                  | producer      |
	| ABC       | 2020          | XYZ  | Arijit Roy,Arkadeep Roy | Bikram Sarkar |
	| DEF       | 2021          | GHI  | Arijit Roy,Arkadeep Roy | Srijit Basu   |