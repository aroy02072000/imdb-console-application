﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imdb.Models;
using Imdb.Repository.Interfaces;
using Imdb.Services.Interfaces;

namespace Imdb.Services
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _actorRepository;

        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }

        // This method is added intentionally for testing purpose (to add some mock data) 
        public void AddMockDataForTesting()
        {
            _actorRepository.AddActors(new Actor() { Name = "Arijit Roy", Dob = new DateTime(2000, 7, 2) });
            _actorRepository.AddActors(new Actor() { Name = "Ankita Datta", Dob = new DateTime(1966, 10, 17) });
            _actorRepository.AddActors(new Actor() { Name = "Soumodip Ghosh", Dob = new DateTime(1982, 8, 15) });
            _actorRepository.AddActors(new Actor() { Name = "Shayani Das", Dob = new DateTime(2010, 3, 17) });
            _actorRepository.AddActors(new Actor() { Name = "Arkadeep Roy", Dob = new DateTime(1998, 5, 3) });
        }

        // This method retrieves all Actor records
        public List<Actor> GetActors() => _actorRepository.GetActors();

        // This method displays all the Actor records in a specific format
        public void ShowActors()
        {
            List<Actor> actorList = _actorRepository.GetActors();
            
            // if no Actor record exists
            if(actorList.Count == 0)
            {
                Console.WriteLine("No Actor record found...");
                return;
            }

            Console.WriteLine("\nActor List: [Actor Age must be atleast 10 with respect to the Year of Release]\n");
            foreach (Actor actor in actorList)
            {
                Console.WriteLine($"{actor.Name} [DOB - {actor.Dob.ToShortDateString()}]");
            }
            Console.WriteLine("");
        }

        // This method can add an Actor record to the database
        public string AddActors(string actorName, string dateOfBirth)
        {
            try
            {
                actorName = actorName.Trim();
                dateOfBirth = dateOfBirth.Trim();

                // if actor name is null
                if (String.IsNullOrEmpty(actorName))
                {
                    throw new Exception("Actor name cannot be empty!");
                }

                // if actor dob is null
                if(String.IsNullOrEmpty(dateOfBirth))
                {
                    throw new Exception("Actor DOB cannot be empty!");
                }

                // check for duplicate records of actor
                DateTime date = Convert.ToDateTime(dateOfBirth + " 00:00:00");
                List<Actor> actorList = _actorRepository.GetActors();
                IEnumerable<Actor> cond1 = actorList.Where(t => t.Dob.Date == date.Date);
                IEnumerable<Actor> cond2 = actorList.Where(t => t.Name == actorName);
                
                // check whether actor dob is after 1899 and he/she has atleast 10 years old
                if(date.Year < 1900 || date.Year > DateTime.Now.Year - 10)
                {
                    throw new Exception($"Actor DOB should be within 1900 and {DateTime.Now.Year - 10}!");
                }

                // if all the conditions validate then add the record
                if (cond1.Count() == 0 && cond2.Count() == 0)
                {
                    Actor newActor = new Actor()
                    {
                        Name = actorName,
                        Dob = date
                    };
                    _actorRepository.AddActors(newActor);
                    return "Database updated successfully...";
                }

                // otherwise duplicate record exists in the database
                else
                {
                    return "Actor details already exists...";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}