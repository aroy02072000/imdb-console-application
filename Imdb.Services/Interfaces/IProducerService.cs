﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imdb.Models;

namespace Imdb.Services.Interfaces
{
    public interface IProducerService
    {
        List<Producer> GetProducers();
        void ShowProducers();
        string AddProducer(string producerName, string dateOfBirth);
    }
}
