﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imdb.Models;

namespace Imdb.Services.Interfaces
{
    public interface IMovieService
    {
        List<List<string>> ShowMovies();
        string AddMovie(string movieName, string yearOfRelease, string plot, List<string> actorNames, string producerName);
        string DeleteMovie(string movieName);
    }
}