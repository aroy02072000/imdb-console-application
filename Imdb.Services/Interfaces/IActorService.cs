﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imdb.Models;


namespace Imdb.Services.Interfaces
{
    public interface IActorService
    {
        List<Actor> GetActors();
        void ShowActors();
        string AddActors(string actorName, string dateOfBirth);
    }
}
