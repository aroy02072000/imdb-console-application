﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imdb.Models;
using Imdb.Repository.Interfaces;
using Imdb.Services.Interfaces;


namespace Imdb.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IActorRepository _actorRepository;
        private readonly IProducerRepository _producerRepository;

        public MovieService(IMovieRepository movieRepository, IActorRepository actorRepository, IProducerRepository producerRepository)
        {
            _movieRepository = movieRepository;
            _actorRepository = actorRepository;
            _producerRepository = producerRepository;
        }

        // This method is added intentionally for testing purpose (to add some mock data)
        public void AddMockDataForTesting()
        {
            _movieRepository.AddMovie
            (
                new Movie() 
                { 
                    Name = "ABC", 
                    YearOfRelease = 2017, 
                    Plot = "XYZ",
                    Actors = new List<Actor>() 
                                   { 
                                        _actorRepository.GetActors().Where(t => t.Name == "Arijit Roy").First(),
                                        _actorRepository.GetActors().Where(t => t.Name == "Arkadeep Roy").First()
                                   },
                    Producers = _producerRepository.GetProducers().Where(t => t.Name == "Bikram Sarkar").First()
                }
            );
        }

        // This method displays all the Movie records in a specific format
        public List<List<string>> ShowMovies()
        {
            List<Movie> movieList = _movieRepository.GetMovies();
            List<List<string>> resultSet = new List<List<string>>();
            List<string> movieRecord = new List<string>();

            // if no Movie record exists
            if (movieList.Count == 0)
            {
                movieRecord.Add("No Movie record found...");
                resultSet.Add(movieRecord);
                return resultSet;
            }

            foreach (Movie movie in movieList)
            {
                List<string> temp = new List<string>();
                temp.Add(movie.Name);
                temp.Add(movie.YearOfRelease.ToString());
                temp.Add(movie.Plot);
                temp.Add(String.Join(',', movie.Actors.Select(t => t.Name).ToList()));
                temp.Add(movie.Producers.Name);
                resultSet.Add(temp);
            }
            return resultSet;
        }

        // This method can add a new Movie record to the database
        public string AddMovie(string movieName, string yearOfRelease, string plot, List<string> actorNames, string producerName)
        {
            try
            {
                movieName = movieName.Trim();
                yearOfRelease = yearOfRelease.Trim();
                plot = plot.Trim();
                producerName = producerName.Trim();

                // movie name is empty
                if (String.IsNullOrEmpty(movieName))
                {
                    throw new Exception("Movie name cannot be empty!");
                }

                // if year of release is empty
                if(String.IsNullOrEmpty(yearOfRelease))
                {
                    throw new Exception("Year of Release cannot be empty!");
                }

                // if there is an invalid year
                int temp;
                if(!int.TryParse(yearOfRelease, out temp))
                {
                    throw new Exception("Invalid Year of Release!");
                }

                // if plot is empty
                if(String.IsNullOrEmpty(plot))
                {
                    throw new Exception("Plot cannot be empty!");
                }

                // if no actor records passed
                if(actorNames.Count() == 0)
                {
                    throw new Exception("Must contain atleast one actor details!");
                }

                // if producer name is empty
                if(String.IsNullOrEmpty(producerName))
                {
                    throw new Exception("Producer name cannot be empty!");
                }

                // check if any invalid year of release (which is out of range) is given or not
                if (DateTime.Now.Year < int.Parse(yearOfRelease) || int.Parse(yearOfRelease) < 1935)
                {
                    throw new Exception($"Year of Release must be between 1935 and {DateTime.Now.Year}!");
                }

                // check whether all the actornames exists in the database
                List<Actor> actorDetails = new List<Actor>();
                actorNames = actorNames.Distinct().ToList();
                List<Actor> actorRecords = _actorRepository.GetActors();
                foreach (var actor in actorNames)
                {
                    string name = actor.Trim();
                    Actor found = actorRecords.Where(t => t.Name == name).FirstOrDefault();
                    if (found != null)
                    {
                        // check whether actor age is atleast 10 years during the year of release
                        if (int.Parse(yearOfRelease) - found.Dob.Year >= 10)
                        {
                            actorDetails.Add(found);
                        }
                        else
                        {
                            throw new Exception("Actor age must be atleast 10 with respect to the Year of Release!");
                        }
                    }
                    else
                    {
                        throw new Exception("Actor record does not exist in the database!");
                    }
                }

                // check whether producer name exists in the database
                List<Producer> producerRecords = _producerRepository.GetProducers();
                Producer producerDetails = producerRecords.Where(t => t.Name == producerName).FirstOrDefault();
                if (producerDetails == null)
                {
                    throw new Exception("Producer record does not exist in the database!");
                }
                else
                {
                    // check whether producer age is atleast 35 years during the year of release
                    if(int.Parse(yearOfRelease) - producerDetails.Dob.Year < 35)
                    {
                        throw new Exception("Producer age must be atleast 35 with respect to the Year of Release!");
                    }
                }

                // check whether a duplicate movie already exists in the database otherwise add the new record
                List<Movie> movieList = _movieRepository.GetMovies();
                if ((movieList.Where(t => t.Name == movieName).ToList().Count() > 0 &&
                     movieList.Where(t => t.Plot == plot).ToList().Count() == 0)    ||
                     movieList.Where(t => t.Name == movieName).ToList().Count() == 0)
                {
                    Movie newMovie = new Movie()
                    {
                        Name = movieName,
                        YearOfRelease = int.Parse(yearOfRelease),
                        Plot = plot,
                        Actors = actorDetails,
                        Producers = producerDetails
                    };
                    _movieRepository.AddMovie(newMovie);
                    return "Database updated successfully...";
                }

                // otherwise duplicate record exists in the database
                else
                {
                    return "Movie details already exists...";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        // This method can delete an existing Movie record from the database
        public string DeleteMovie(string movieName)
        {
            try
            {
                movieName = movieName.Trim();

                // if moie name is null
                if (String.IsNullOrEmpty(movieName))
                {
                    throw new Exception("Movie name cannot be empty!");
                }

                // search for the movie in the database; if found then delete it
                List<Movie> movieList = _movieRepository.GetMovies();
                foreach (Movie movie in movieList)
                {
                    if (movie.Name == movieName)
                    {
                        _movieRepository.DeleteMovie(movieName);
                        return "Deleted successfully...";
                    }
                }
                return "Record not found...";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}