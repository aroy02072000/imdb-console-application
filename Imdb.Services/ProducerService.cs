﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imdb.Models;
using Imdb.Repository.Interfaces;
using Imdb.Services.Interfaces;


namespace Imdb.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _producerRepository;

        public ProducerService(IProducerRepository producerRepository)
        {
            _producerRepository = producerRepository;   
        }

        // This method is added intentionally for testing purpose (to add some mock data) [not the part of actual project]
        public void AddMockDataForTesting()
        {
            _producerRepository.AddProducer(new Producer() { Name = "Bikram Sarkar", Dob = new DateTime(1956, 1, 23) });
            _producerRepository.AddProducer(new Producer() { Name = "Srijit Basu", Dob = new DateTime(1967, 2, 17)});

        }

        // This method retrieves all the Producer records
        public List<Producer> GetProducers() => _producerRepository.GetProducers();

        // This method displays all the Producer records in a specific format
        public void ShowProducers()
        {
            List<Producer> producerList = _producerRepository.GetProducers();
            
            // if no record exists
            if (producerList.Count == 0)
            {
                Console.WriteLine("No Producer record found...");
                return;
            }

            Console.WriteLine("\nProducer List: [Producer Age must be atleast 35 with respect to the Year of Release]\n");
            foreach (Producer producer in producerList)
            {
                Console.WriteLine($"{producer.Name} [DOB - {producer.Dob.ToShortDateString()}]");
            }
            Console.WriteLine("");
        }

        // This method adds a new Producer record to the database
        public string AddProducer(string producerName, string dateOfBirth)
        {
            try
            {
                producerName = producerName.Trim();
                dateOfBirth = dateOfBirth.Trim();
                
                // if producer name is null
                if (String.IsNullOrEmpty(producerName))
                {
                    throw new Exception("Producer name cannot be empty!");
                }

                // if producer DOB is null
                if(String.IsNullOrEmpty(dateOfBirth))
                {
                    throw new Exception("Producer DOB cannot be empty!");
                }

                // check whether any duplicate record exists
                List<Producer> producerList = _producerRepository.GetProducers();
                DateTime date = Convert.ToDateTime(dateOfBirth + " 00:00:00");
                IEnumerable<Producer> cond1 = producerList.Where(t => t.Dob.Date == date.Date);
                IEnumerable<Producer> cond2 = producerList.Where(t => t.Name == producerName);
                
                // check whether dob is within the range and the age is atleast 35 with respect to the year of release
                if (date.Year < 1900 || date.Year > DateTime.Now.Year - 35)
                {
                    throw new Exception($"Producer DOB should be within 1900 and {DateTime.Now.Year - 35}!");
                }

                // check whether all conditions validated
                if (cond1.Count() == 0 && cond2.Count() == 0)
                {
                    Producer newProducer = new Producer()
                    {
                        Name = producerName,
                        Dob = DateTime.ParseExact(dateOfBirth, "dd/mm/yyyy", null)
                    };
                    _producerRepository.AddProducer(newProducer);
                    return "Database updated successfully...";
                }

                // otherwise duplicate record exists
                else
                {
                    return "Producer details already exists...";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}