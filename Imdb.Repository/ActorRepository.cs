﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imdb.Models;
using Imdb.Repository.Interfaces;

namespace Imdb.Repository
{
    public class ActorRepository : IActorRepository
    {
        private readonly List<Actor> _actors;

        public ActorRepository()
        {
            _actors = new List<Actor>();
        }

        public List<Actor> GetActors()
        {
            return _actors;
        }

        public void AddActors(Actor newActor)
        {
            _actors.Add(newActor);
        }
    }
}