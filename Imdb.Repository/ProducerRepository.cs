﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imdb.Models;
using Imdb.Repository.Interfaces;

namespace Imdb.Repository
{
    public class ProducerRepository : IProducerRepository
    {
        private readonly List<Producer> _producers;

        public ProducerRepository()
        {
            _producers = new List<Producer>();
        }
    
        public List<Producer> GetProducers()
        {
            return _producers;
        }

        public void AddProducer(Producer newProducer)
        {
            _producers.Add(newProducer);
        }
    }
}