﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imdb.Models;
using Imdb.Repository.Interfaces;

namespace Imdb.Repository
{
    public class MovieRepository : IMovieRepository
    {
        private readonly List<Movie> _movies;

        public MovieRepository()
        {
            _movies = new List<Movie>();
        }

        public List<Movie> GetMovies()
        {
            return _movies; 
        }

        public void AddMovie(Movie newMovie)
        {
            _movies.Add(newMovie);
        }

        public void DeleteMovie(string movieName)
        {
            _movies.RemoveAll(t => t.Name == movieName);
        }
    }
}








