﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imdb.Models;

namespace Imdb.Repository.Interfaces
{
    public interface IActorRepository
    {
        List<Actor> GetActors();
        void AddActors(Actor newActor);
    }
}
