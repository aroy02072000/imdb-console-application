﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imdb.Models
{
    public class Producer
    {
        public string Name { get; set; }
        public DateTime Dob { get; set; }
    }
}
