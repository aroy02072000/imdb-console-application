﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imdb.Models;
using Imdb.Repository;
using Imdb.Services;

namespace Imdb
{
    public class Program
    {
        private static readonly ActorService _actorService;
        private static readonly ProducerService _producerService;
        private static readonly MovieService _movieService;

        // static constructor used to initialize services
        static Program()
        {
            ActorRepository actorRepository = new ActorRepository();
            ProducerRepository producerRepository = new ProducerRepository();
            _actorService = new ActorService(actorRepository);
            _producerService = new ProducerService(producerRepository);
            _movieService = new MovieService(new MovieRepository(), actorRepository, producerRepository);
        }

        // main method for console UI
        static void Main(string[] args)
        {
            try
            {
                // menu to be displayed on the console
                while (true)
                {
                    Console.WriteLine("\n----------------------------------------------------------------\n");
                    Console.WriteLine("Press 0 - Exit");
                    Console.WriteLine("Press 1 - List Movies");
                    Console.WriteLine("Press 2 - Add Movie");
                    Console.WriteLine("Press 3 - Add Actor");
                    Console.WriteLine("Press 4 - Add Producer");
                    Console.WriteLine("Press 5 - Delete Movie");
                    Console.Write("\nEnter your choice: ");
                    string choice = Console.ReadLine();

                    switch (choice)
                    {

                        // case: close the console UI
                        case "0":
                            Console.WriteLine("Console application closed successfully...");
                            return;

                        // case: want to list all the existing movie records
                        case "1":
                            Console.WriteLine("");
                            List<List<string>> resultSet = _movieService.ShowMovies();
                            if(resultSet.First().ToList().Count == 1)
                            {
                                Console.WriteLine(resultSet.First().First().ToString());
                            }
                            else
                            {
                                foreach(var movieRecords in resultSet)
                                {
                                    Console.WriteLine($"Movie Name - {movieRecords[0]}");
                                    Console.WriteLine($"Year of Release - {movieRecords[1]}");
                                    Console.WriteLine($"Plot - {movieRecords[2]}");
                                    Console.WriteLine($"Actor(s) - {movieRecords[3]}");
                                    Console.WriteLine($"\nProducer - {movieRecords[4]}");
                                }
                            }
                            continue;

                        // case: want to add a new movie record
                        case "2":

                            // if no actor record is there then add some actor record(s)
                            if (_actorService.GetActors().Count == 0)
                            {
                                Console.WriteLine("\nNo Actor record found! First add some Actor record(s)...");
                                continue;
                            }

                            // if no producer record is there then add some producer record(s)
                            if (_producerService.GetProducers().Count == 0)
                            {
                                Console.WriteLine("\nNo Producer record found! First add some Producer record(s)...");
                                continue;
                            }

                            // add the movie name
                            Console.Write("\nEnter Movie name: ");
                            string movieName = Console.ReadLine();

                            // add the year of release of this movie
                            Console.Write("Enter year of release: ");
                            string yearOfRelease = Console.ReadLine();

                            // add the plot of this movie
                            Console.Write("Enter plot of the movie: ");
                            string plot = Console.ReadLine();

                            // add atleast one actor of this movie
                            List<string> actorNames = new List<string>();
                            while (true)
                            {
                                _actorService.ShowActors();
                                Console.Write("Enter Actor name (listed above): ");
                                actorNames.Add(Console.ReadLine());
                                Console.Write("Do you want to add another Actor [Y/Any Key - then press ENTER]: ");
                                string ch = Console.ReadLine();
                                if (ch == "Y" || ch == "y")
                                {
                                    continue;
                                }
                                else
                                {
                                    break;
                                }
                            }

                            // add one producer of this movie
                            _producerService.ShowProducers();
                            Console.Write("Enter Producer name (listed above): ");
                            string producerName = Console.ReadLine();
                            Console.WriteLine(_movieService.AddMovie(movieName, yearOfRelease, plot, actorNames, producerName));
                            continue;

                        // case: want to add a new actor record
                        case "3":
                            Console.Write("\nEnter the actor name: ");
                            string actorName = Console.ReadLine();
                            Console.Write($"Enter the DOB (in dd/mm/yyyy format) [Between 1900 and {DateTime.Now.Year - 10}]: ");
                            string dateOfBirth = Console.ReadLine();
                            Console.WriteLine(_actorService.AddActors(actorName, dateOfBirth));
                            continue;

                        // case: want to add a new producer record
                        case "4":
                            Console.Write("\nEnter producer name: ");
                            producerName = Console.ReadLine();
                            Console.Write($"Enter the DOB (in dd/mm/yyyy format) [Between 1900 and {DateTime.Now.Year - 35}]: ");
                            dateOfBirth = Console.ReadLine();
                            Console.WriteLine(_producerService.AddProducer(producerName, dateOfBirth));
                            continue;

                        // case: want to delete an existing movie record
                        case "5":
                            Console.Write("\nEnter the movie name to be deleted: ");
                            movieName = Console.ReadLine();
                            Console.WriteLine(_movieService.DeleteMovie(movieName));
                            continue;
                        
                        // case: if any wrong choice is selected
                        default:
                            Console.WriteLine("You've entered a wrong choice!");
                            continue;
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid Input!");
            }
        }
    }
}